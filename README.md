# ATAC 2021 Submission: Collaborative Route Finding in Semantic Mazes

Project for [ATAC-2021](https://all-agents-challenge.github.io/atac2021/): submission from Katharine Beaumont, Eoin O’Neill, Nestor Velasco Bermeo, Rem Collier of University College Dublin, Ireland.

## Video

[The demo video is here](https://youtu.be/b2tecNJc0DE). It has subtitles for your convenience.

## Contents

This folder includes:
- [maze-server](https://github.com/all-agents-challenge/maze-server)
- [atac-query-manager](https://gitlab.com/mams-ucd/atac-query-manager)
- [atac-maze-agents](https://gitlab.com/mams-ucd/atac-2021)

The maze-server is the server supplied with the challenge.
The atac-query-manager provides a UI interface that accepts room names as a query, and passes to the atac-maze-agents, the agent-based system that use reinforcement learning and collaboration to search for rooms in the maze. See the architecture diagram below.

All of the above can be downloaded in one step through the following git command:

```
git clone --recursive https://gitlab.com/mams-ucd/atac-maze.git
```

The `--recursive` flag causes git to pull the dependant sub-modules.

Then:
- Navigate into the maze-server subfolder.
- Run the [RDF version](https://github.com/all-agents-challenge/maze-server) with command `node app-rdf.js`
- Open a new terminal and navigate into the atac-query-manager subfolder
- Run `mvn install` and then `mvn spring-boot:run`
- Open a new terminal and navigate into the atac-maze-agents subfolder
- Run `mvn` 
- In a browser, navigate to http://localhost:9010/

For more detailed information on the agent code, see this [README](https://gitlab.com/mams-ucd/atac-2021).

## Viewing the code

- This code contains all the application specific code for the submission
- It also uses parts of of our Multi-Agent Microservices (MAMS) CArtAgO implementation, which can be found [here](https://gitlab.com/mams-ucd/mams-cartago)
 - The [mams-astra-jena](https://gitlab.com/mams-ucd/mams-cartago/-/tree/master/mams-astra-jena) git module has been developed explicitly for ATAC 2021.
 - It provides the Apache Jena / ASTRA integration, support for generating turtle code, and support for reasoning using RDFSchema.

## Architecture diagram

![DT](/images/Sys diag.png)

## Paper

[The paper submission is here](https://gitlab.com/mams-ucd/atac-maze/-/blob/master/Collaborative%20Route%20Finding%20in%20Semantic%20Mazes.pdf)

